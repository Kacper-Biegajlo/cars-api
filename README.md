# Deployment of a Java application to AWS, using AWS S3 and AWS Elastic Beanstalk.

- Automatic deployments using AWS
- Pipelines with code quality checks, unit tests, API testing

  <details> 
    <summary markdown="span">HTML artifact reports from last deployment</summary>

    https://gitlab-projects6.gitlab.io/-/cars-api/-/jobs/3116198259/artifacts/newman/report.html
    
    https://gitlab-projects6.gitlab.io/-/cars-api/-/jobs/3116198257/artifacts/build/reports/tests/test/index.html

    https://gitlab-projects6.gitlab.io/-/cars-api/-/jobs/3116198256/artifacts/build/reports/pmd/main.html
  
  </details>
